using System;
using System.Collections.Generic;
using StateSystem;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Core
{
    public class TestInstaller : MonoInstaller
    {
        [SerializeField] private ShootView shootView;
        [SerializeField] private GameObject root;
        //private Dictionary<Type, AState> someDictionary;
        public override void InstallBindings()
        {
            Container.Bind<ShootView>().FromInstance(shootView).AsSingle().NonLazy();
            //Container.Bind<StateMachine>().AsSingle();
            Container.Bind<int>().FromComponentOn(root).AsSingle().NonLazy();
            #region States
            Container.Bind<AState>()
                .WithId(BindId.INVISIBLE_STATE)
                .To<InvisibleState>()
                .AsSingle()
                .NonLazy();
            
            Container.Bind<AState>()
                .WithId(BindId.SHOOT_STATE)
                .To<ShootState>()
                .AsSingle()
                .NonLazy();
            
            //Container.Bind<Dictionary<Type, AState>>().FromInstance(someDictionary).AsSingle();
            #endregion
            
            Container.Bind<IStateMachine>().To<StateMachine>().AsSingle().NonLazy();
        }
    }

    public static class BindId
    {
        public const uint INVISIBLE_STATE = 0;
        public const uint SHOOT_STATE = 1;

    }
}