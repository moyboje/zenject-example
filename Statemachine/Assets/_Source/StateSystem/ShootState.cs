using UnityEngine;
using Zenject;

namespace StateSystem
{
  public class ShootState : AState
  {
    [Inject]
    public ShootState(Transform root)
    {
      Debug.Log(root);
    }

    public override void Enter()
    {
      Debug.Log("Enter ShootState");
    }

    public override void Update()
    {
      if (Input.GetKeyDown(KeyCode.A))
        Debug.Log("A pressed in ShootState");
    }

    public override void Exit()
    {
      Debug.Log("Exit ShootState");
    }
  }
}