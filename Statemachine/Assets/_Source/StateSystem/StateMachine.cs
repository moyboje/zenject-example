﻿using System;
using System.Collections.Generic;
using Core;
using Zenject;

namespace StateSystem
{
  public class StateMachine : IStateMachine
  {
    private readonly Dictionary<Type, AState> _states;
    private AState _currentState;

    [Inject]
    public StateMachine([Inject(Id = BindId.INVISIBLE_STATE)] AState invisibleState,
      [Inject(Id = BindId.SHOOT_STATE)] AState shootState)
    {
      _states = new Dictionary<Type, AState>()
      {
        { typeof(InvisibleState), invisibleState },
        { typeof(ShootState), shootState }
      };

      foreach (var state in _states)
        state.Value.SetOwner(this);
    }
    
    public void ChangeState(Type type)
    {
      _currentState?.Exit();
      _currentState = _states[type];
      _currentState.Enter();
    }

    public void Update()
    {
      _currentState?.Update();
    }
  }
}