﻿using System;

namespace StateSystem
{
    public interface IStateMachine
    {
        void ChangeState(Type type);
        void Update();
    }
}