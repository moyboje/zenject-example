namespace StateSystem
{
  public abstract class AState
  {
    protected IStateMachine Owner;

    public void SetOwner(IStateMachine owner)
    {
      Owner = owner;
    }
  
    public virtual void Enter() {}
    public virtual void Update() {}
    public virtual void Exit() {}
  }
}